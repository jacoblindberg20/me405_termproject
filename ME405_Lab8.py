'''
@file ME405_Lab8.py

@brief Contains the motor and encoder drivers for the ME305/405 board motors and encoders.

@author Dakota Baker
@date March 6, 2021
'''

import pyb
from math import floor
from sys import exit
import micropython

# Allocates 200 bytes for an emergency error message
micropython.alloc_emergency_exception_buf(200)

class Encoder:
    '''
    @brief Sets-up the encoder on the Nucelo and reads the encoder's position
    @details Using the user defined PPR, max rpm, and counter size, an appropriate time 
             period is calculated to run this class to read the encoder's position.
             Overflow (max counter range to 0) and underflow (0 to max counter range)
             are accounted for by checking the difference between readings and 
             making the appropriate corrections if it is > 0.5*counter_range.
    '''
    
    def __init__(self, timer, pin1, pin1_ch, pin2, pin2_ch, counter_size, PPR, rpm):      
        '''
        @brief Defines the encoder on the Nucelo, the time period to run, and initializes the finite state machine
        @param timer        Timer object used with pyb.Timer()
        @param pin1         Pin object 1 for the connection of the encoder to the Nucelo
        @param pin1_ch      Channel on the Timer object used for Pin object 1
        @param pin2         Pin object 2 for the connection of the encoder to the Nucelo
        @param pin2_ch      Channel on the Timer object used for Pin object 2
        @param counter_size Size of the processor's timer in bits
        @param PPR          Pulses per revolution of the encoder
        @param rpm          Maximum revolutions per minute of the motor
        '''
        
        ## Timer object
        self.timer = timer
        ## Pin1 object
        self.pin1 = pin1
        ## Pin1 Channel Object
        self.pin1_ch = pin1_ch
        ## Pin2 Object
        self.pin2 = pin2
        ## Pin2 Channel Object
        self.pin2_ch = pin2_ch
        ## Creates object for Pulses per Revolution
        self.PPR = PPR
        
        ## Defines the range of the counter before overflow
        self.counter_range = 2**(counter_size) - 1  
        
        ## Sets-up a timer object for the encoder
        self.tim = pyb.Timer(timer)
        #print('tim = pyb.Timer(' + str(timer) +')');
        
        ## Defines the timer object to counter every pulse for the desired counter range
        self.tim.init(prescaler = 0, period = self.counter_range)
        #print('tim.init(prescaler = 0, period = {:})'.format(period))
        
        ## Defines the timer channel for Pin object 1
        self.tim.channel(pin1_ch, pin = pin1, mode = pyb.Timer.ENC_AB)
        #print('tim.channel(' + str(pin1_ch) + ', pin = pyb.Pin.cpu.' + str(pin1) + ', mode = pyb.Timer.ENC_AB)')
        
        ## Defines the timer channel for Pin object 2
        self.tim.channel(pin2_ch, pin = pin2, mode = pyb.Timer.ENC_AB)
        #print('tim.channel(' + str(pin2_ch) + ', pin = pyb.Pin.cpu.' + str(pin2) + ', mode = pyb.Timer.ENC_AB)')
    
        ######################################################################        
        
        ## A constant defining the shortest time interval necessary to read the encoder's position without errors
        self.period_max = round((0.5*(2**counter_size) / (PPR*rpm/60))*1e3) # period necessary to run encoder in miliseconds
        
        ## Total displacement of the output shaft. This is the sum of all the "delta's" since the encoder was zeroed 
        self.position = 0
        
        ## Position of the encoder (in ticks) of the last calling of tim.counter() before the present position of the encoder
        self.previous_position = 0
        
        ## Constant used to alter current position 
        self.offset_position = 0

    def update(self):
        '''
        @brief Updates the encoder's total displaced position
        @details The current position (in ticks) is called and after calculating
                 the difference between the current and previous reading (accounting
                 for under- and overflow)* the total displacement is updated.
                 
                 *If the difference is greater than 0.5*counter_range,
                 the delta is corrected, depending on whether it is positive or
                 negative, by subtracting or adding the counter_range, respectively.
        '''
        ## Reading of the encoder (in ticks) from the tim.counter() command
        self.current_position = self.tim.counter() + self.offset_position
        ## Constant used to determine how many overflows the tick counter has passed.
        # The main concern is if the offset position is unreasonably high or low.
        self.i = floor(self.current_position / self.counter_range)
        if self.current_position > abs(self.counter_range*self.i):
            self.current_position = self.current_position - self.counter_range*self.i
        # print('Previous position:    {:}'.format(self.previous_position))
        # print('Got current position: {:0.0f}'.format(self.current_position))
        
        ## Difference in the tick value of the current position from the previous reading of the encoder
        self.delta = self.current_position - self.previous_position
        # print('Got delta')
        
        self.previous_position = self.current_position
        
        if abs(self.delta) > 0.5*self.counter_range:
            #print('Bad delta = {:}'.format(self.delta))
            if self.delta > 0:
                self.delta -= self.counter_range
                #print('Corrected delta')
            elif self.delta < 0:
                self.delta += self.counter_range
        else:
            #print('Good delta')
            pass
        
        self.position += self.delta
                    
        
    def get_delta(self):
        '''
        @brief Returns the difference between the two most recent calls of the "update" function
        '''
        return self.delta
    
    
    def get_position(self):
        '''
        @brief Returns the encoder's total shaft displacement (in degrees)
        '''
        #Displays the current encoder position (in ticks) and total shaft displacement (in degrees) to the user interface
        return self.position*360/self.PPR
        # print('Encoder counter: {:}'.format(self.current_position))
        # print('Total Shaft position: {:0.2f} deg'.format(self.position*360/(self.PPR)))
        #print('------------------------------------------------')
    
    def set_position(self, new_position):
        '''
        @brief Sets encoder counter and total shaft position to a given value. For example, passing in 0 will zero the encoder position.
        '''
        self.current_position = int(new_position*self.PPR/360)
        self.position = self.current_position
        self.i = floor(self.current_position / self.counter_range)
        if self.current_position > self.counter_range*self.i:
            self.current_position = self.current_position - self.counter_range*self.i
        self.previous_position = self.current_position
        self.offset_position = int(new_position*self.PPR/360)
        # Reset counter
        self.tim = pyb.Timer(self.timer)
        self.tim.init(prescaler = 0, period = self.counter_range)
        self.tim.channel(self.pin1_ch, pin = self.pin1, mode = pyb.Timer.ENC_AB)
        self.tim.channel(self.pin2_ch, pin = self.pin2, mode = pyb.Timer.ENC_AB)


###############################################################################
###############################################################################
###############################################################################


class Motor:
    ''' 
    @brief This class is a motor driver for the ME305/405 board motors. 
    '''


    def __init__(self, nSLEEP_pin, IN1_pin, IN1_ch, IN2_pin, IN2_ch, timer, Mot_num, dbg):
        ''' 
        @brief Creates a motor driver by initializing GPIO pins and turning the motor off for safety.
        @param nSLEEP_pin A pyb.Pin object to use as the enable pin.
        @param IN1_pin    A pyb.Pin object to use as the input to half bridge 1.
        @param IN1_ch     A channel object for IN1_pin.
        @param IN2_pin    A pyb.Pin object to use as the input to half bridge 2.
        @param IN2_ch     A channel object for IN2_pin.
        @param timer      A pyb.Timer object to use for PWM generation on IN1_pin and IN2_pin.
        @param Mot_num    1 = Motor A, 0 = Motor B. Used to initialize nFault interrupt for only 1 motor.
        @param dbg        1 = Print debug statements, 0 = No debug statements
        '''
        
        ## Object for pinA15 on the Nucleo. Initialized as push-pull (on/off)
        self.pin15 = pyb.Pin(nSLEEP_pin, mode = pyb.Pin.OUT_PP)
        
        # Disable motors by default
        self.pin15.low()
        
        ## Object for IN1 using a Nucleo pin to control Motor 1
        self.IN1 = pyb.Pin(IN1_pin)
        
        ## Object for IN2 using a Nucleo pin to control Motor 2
        self.IN2 = pyb.Pin(IN2_pin)
        
        ## Object for timer that controls motor inputs
        self.timer = pyb.Timer(timer, freq=20000)
        
        ## Object for the timer channel used with IN1
        self.timer_ch_IN1 = self.timer.channel(IN1_ch, pyb.Timer.PWM, pin = self.IN1)
        
        ## Object for the timer channel used with IN2
        self.timer_ch_IN2 = self.timer.channel(IN2_ch, pyb.Timer.PWM, pin = self.IN2)
        
        ## A variable that communicates if the Motor has an error.
        self.nFault = False
        
        ## Object for debug statements
        self.dbg = dbg

        if self.dbg:
            print('Creating a motor driver ')
        
        # Only define the nFault interrupt for Motor A
        # if Mot_num == 1:
            ## Initializes an external interrupt on the falling edge for PinB2 (nFault)
        self.ExtInterrupt = pyb.ExtInt(pyb.Pin.cpu.B2,
                       pyb.ExtInt.IRQ_FALLING, 
                       pyb.Pin.PULL_NONE, 
                       callback = self.ISR)
        
        # else:
        #     pass
        
    
    def ISR(self, pinSource):
        '''
        @brief Function that runs when the nFault pin is pulled low
        @details When the nFault pin (B2) is pulled low, there is an error
                 with the motors. The nFault variable is then set to True.
                 Whatever program is running the Motor Driver should detect
                 if nFault is True and then run the Error function.
        '''
        self.nFault = True

            
    def Error(self):
        '''
        @brief Shuts down motors & prompts user if they want to continue operation.
        @details The motors are disabled and then a print statement informs the user
                 of possible motor errors. The user is prompted if they want to continue
                 operation. If no, the program exits. If yes, the motors are re-enabled.
        '''
        self.disable()
        # pinA5 = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
        # pinA5.low()
        print('----------------------------------------------\n' +
              'MOTOR ERROR!\n' + 
              'Note: This could be due to a current overload,\n' +
              'overheating, and/or a short-circuit.') 
        ## User input to continue or discontinue motor operation.
        user_input = ''
        ## A lsit of possible answers to continue motor operation.
        answers = ['Y', 'Yes', 'YES', 'y', 'yes']
        ## A lsit of possible answers to discontinue motor operation.
        rejections = ['N', 'No', 'NO', 'n', 'no']
        while user_input not in answers:
            user_input = input('Resume Operation (Y/N)? ')
            if user_input in answers:
                print('Motor Enabled')
                print('----------------------------------------------')
                self.enable()
                self.nFault = False
                #pinA5.high()
            elif user_input in rejections:
                print('----------------------------------------------')
                exit('Program Aborted')
            else:
                pass
        
    def enable(self):
        '''
        @brief Initializes motors by setting pinA15 to high() 
        '''
        self.ExtInterrupt.disable()
        self.pin15.high()
        pyb.delay(1)
        self.ExtInterrupt.enable()
        # Motors not moving upon activation
        self.timer_ch_IN1.pulse_width_percent(0)
        self.timer_ch_IN2.pulse_width_percent(0)
        if self.dbg:
            print ('Enabling Motor ')

    def disable(self):
        '''
        @brief Disables motors by setting pinA15 to low()
        '''
        self.pin15.low()
        if self.dbg:
            print ('Disabling Motor')

    def set_duty(self, duty):
        '''
        @brief   Sets the duty cycle to be sent to the motors (affects motor speed)
        @details This method sets the duty cycle to be sent to the motors (affects
                 motor speed) to the given level. Positive values cause effort 
                 in one direction, negative values in the opposite direction.
        @param duty A signed integer holding the duty cycle of the PWM signal sent to the motor 
        '''
        if duty > 0:
            self.timer_ch_IN2.pulse_width_percent(0)
            self.timer_ch_IN1.pulse_width_percent(duty)
            if self.dbg:
                print('Motor spinning forwards (CCW)')
        elif duty < 0:
            self.timer_ch_IN1.pulse_width_percent(0)
            self.timer_ch_IN2.pulse_width_percent(abs(duty))
            if self.dbg:
                print('Motor spinning backwards (CW)')
        else:
            self.timer_ch_IN1.pulse_width_percent(duty)
            self.timer_ch_IN2.pulse_width_percent(duty)
            if self.dbg:
                print('Motor coasting to a stop')

