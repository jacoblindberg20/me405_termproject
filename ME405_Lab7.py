'''
@file ME405_Lab7.py

@brief Collects touchscreen data.
@details Uses a TFT LCD Display (model: ER-TFT080-1) to track the motion of a ball
         on a rotating platform. Location is displayed as + or - from the center in mm.
@author Dakota Baker
@date February 24, 2021
'''

from pyb import Pin
from pyb import ADC
# import utime

class TouchScreen:
    '''
    @brief Reads a TFT LCD Display (touchscreen).
    @details Uses a TFT LCD Display (model: ER-TFT080-1) to track the motion of a ball
             on a rotating platform in the x, y, & z (there is no contact) directions.
             Location is displayed as + or - from the center in mm. The readings use 
             a 4-way voltage divider, so each direction (x,y,z) must be read one at 
             a time.
    '''
    
    ## Maximum value of the ADC module
    ADC_count = 2**12 - 1
    
    def __init__(self, xm, xp, ym, yp, length, width):
        '''
        @brief Creates class objects of input variables
        @param xm Pin object for the x_minus reading
        @param xp Pin object for the x_plus reading
        @param ym Pin object for the y_minus reading
        @param yp Pin object for the y_plus reading
        @param length Length of the touchscreen (x direction)
        @param width Width of the touchscreen (y direction)
        '''
        # start_time = utime.ticks_us()
        
        ## Class object for the x_minus pin
        self.pin_xm = xm
        ## Class object for the x_plus pin
        self.pin_xp = xp
        ## Class object for the y_minus pin
        self.pin_ym = ym
        ## Class object for the y_plus pin
        self.pin_yp = yp
        ## Class object for the center point of the touchscreen in the x direction
        self.xc = length/2   # [mm]
        ## Class object for the center point of the touchscreen in the y direction
        self.yc = width/2    # [mm]
        ## Conversion from the ADC reading to milimmeters in the x direction
        self.X_convert = length/self.ADC_count
        ## Conversion from the ADC reading to milimmeters in the y direction
        self.Y_convert = width/self.ADC_count
        
        # end_time = utime.ticks_us()
        # total_time = utime.ticks_diff(end_time, start_time)
        # print(total_time)
        
    def Scan_X(self):
        '''
        @brief Reads the x location on the touchscreen
        '''
        # start_time = utime.ticks_us()
        
        ## Initialize x_minus pin low
        self.pin_xm.init(mode = Pin.OUT_PP, value = 0)
        ## Initialize x_plus pin high
        self.pin_xp.init(mode = Pin.OUT_PP, value = 1)
        ## Initialize y_plus pin as input (floating)
        self.pin_yp.init(mode = Pin.IN)

        ## Initialize y_minus pin as an analog input
        self.pin_ym.init(mode = Pin.ANALOG)
        ADC_ym = ADC(self.pin_ym)
        
        reading = 0
        for n in range(3):
            reading += ADC_ym.read()
        return (reading/3)*self.X_convert - self.xc
        # end_time = utime.ticks_us()
        # total_time = utime.ticks_diff(end_time, start_time)
        # print(str(reading-self.xc) + ' {:}'. format(total_time))
        # while True:
        #     print(ADC_ym.read())
            
    def Scan_Y(self):
        '''
        @brief Reads the y location on the touchscreen
        '''
        # start_time = utime.ticks_us()
        
        ## Initialize x_plus pin as input (floating)
        self.pin_xp.init(mode = Pin.IN)
        ## Initialize y_minus pin low
        self.pin_ym.init(mode = Pin.OUT_PP, value = 0)
        ## Initialize y_plus pin high
        self.pin_yp.init(mode = Pin.OUT_PP, value = 1)

        ## Initialize x_minus pin as an analog input
        self.pin_xm.init(mode = Pin.ANALOG)
        ADC_xm = ADC(self.pin_xm)
        
        reading = 0
        for n in range(3):
            reading += ADC_xm.read()
        return (reading/3)*self.Y_convert - self.yc
        # end_time = utime.ticks_us()
        # total_time = utime.ticks_diff(end_time, start_time)
        # print(str(reading-self.yc) + ' {:}'. format(total_time))
        # while True:
        #     print(ADC_xm.read())
            
    def Optimized_Scan_Y(self):
        '''
        @brief Optimized scanning of y ONLY used in Scan_XYZ function
        '''
        ## Initialize y_minus pin low
        self.pin_ym.init(mode = Pin.OUT_PP, value = 0)

        ## Initialize x_minus pin as an analog input
        self.pin_xm.init(mode = Pin.ANALOG)
        ADC_xm = ADC(self.pin_xm)
        
        reading = 0
        for n in range(3):
            reading += ADC_xm.read()
        return (reading/3)*self.Y_convert - self.yc
        
    def Scan_Z(self):
        '''
        @brief Reads the z location (is there contact? True/False) on the touchscreen
        '''
        # start_time = utime.ticks_us()
        
        ## Initialize x_minus pin low
        self.pin_xm.init(mode = Pin.OUT_PP, value = 0)
        ## Initialize x_plus pin as input (floating)
        self.pin_xp.init(mode = Pin.IN)
        ## Initialize y_plus pin high
        self.pin_yp.init(mode = Pin.OUT_PP, value = 1)

        ## Initialize y_minus pin as an analog input
        self.pin_ym.init(mode = Pin.ANALOG)
        ADC_ym = ADC(self.pin_ym)
        
        if ADC_ym.read() > 4080:
            return False
            # end_time = utime.ticks_us()
            # total_time = utime.ticks_diff(end_time, start_time)
            # print('False, {:}'.format(total_time))
        else:
            return True
            # end_time = utime.ticks_us()
            # total_time = utime.ticks_diff(end_time, start_time)
            # print('True, {:}'.format(total_time))
        # while True:
        #     if ADC_ym.read() > 4080:
        #         print('False')
        #     else:
        #         print('True')
        
    def Scan_XYZ(self):
        '''
        @brief Reads the x, y, & z output of the touchscreen and returns a tuple (Z, Y, X)
        '''
        # start_time = utime.ticks_us()  # The number of miliseconds since arbitrary starting point
                
        ## Tuple of the x, y, & z outputs of the touchscreen
        self.scan_tup = (self.Scan_Z(), self.Optimized_Scan_Y(), self.Scan_X())
        return self.scan_tup

        # end_time = utime.ticks_us()
        # total_time = utime.ticks_diff(end_time, start_time)
        # print('{:}, {:}'.format(self.scan_tup, total_time))
        # return total_time

