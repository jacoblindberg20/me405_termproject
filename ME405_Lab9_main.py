'''
@file ME405_Lab9_main.py

@brief This file controls the ME305/405 balancing platform

@authors Dakota Baker & Jacob Lindberg
@date March 16, 2021
'''

import pyb
from ME405_Lab8 import Motor
from ME405_Lab8 import Encoder
from ME405_Lab7 import TouchScreen
import machine
from bno055 import *
import micropython

# Allocates 200 bytes for an emergency error message
micropython.alloc_emergency_exception_buf(200)

''' Define Encoder parameters '''

## Pyboard timer object
timer_ENC_A = 4

## Pin object 1
pin1 = pyb.Pin(pyb.Pin.cpu.B6)

## Channel of timer for Pin Object 1
pin1_ch = 1

## Pin Object 2
pin2 = pyb.Pin(pyb.Pin.cpu.B7)

## Channel of timer for Pin Object 2
pin2_ch = 2

## Pyboard timer object
timer_ENC_B = 8

## Pin object 3
pin3 = pyb.Pin(pyb.Pin.cpu.C6)

## Channel of timer for Pin Object 3
pin3_ch = 1

## Pin Object 4
pin4 = pyb.Pin(pyb.Pin.cpu.C7)

## Channel of timer for Pin Object 4
pin4_ch = 2

## Size of counter (bits). The Nucelo has a 16-bit counter
counter_size = 16 # bit size

## Pulse per revolution for user's encoder
PPR = 4000 # Pulse per revolution

## Maximum expected revolutions per minute for motor (with a load)
rpm = 10800 # nominal rpm of motor (Note: output shaft will be 4x faster)

''' Define Motor parameters '''

## Pin object used to enable the motor
nSLEEP_pin = pyb.Pin.cpu.A15

## Pin object for input 1 to half-bridge 
IN1_pin = pyb.Pin.cpu.B4

## Pin object for input 2 channel to half-bridge
IN1_ch = 1

## Pin object for input 2 to half-bridge 
IN2_pin = pyb.Pin.cpu.B5

## Pin object for input 2 channel to half-bridge
IN2_ch = 2

## Pin object for input 3 to half-bridge 
IN3_pin = pyb.Pin.cpu.B0

## Pin object for input 3 channel to half-bridge
IN3_ch = 3

## Pin object for input 4 to half-bridge 
IN4_pin = pyb.Pin.cpu.B1

## Pin object for input 4 channel to half-bridge
IN4_ch = 4

## Create the timer object used for PWM generation
timer_MOT = 3

## Create the debug object. 1 = print debug statements, 0 = no debug statements
dbg = 1

## Defines Motor A so the nFault interrupt only gets defined once.
Mot_num_A = 1

## Defines Motor B so the nFault interrupt only gets defined for Motor A.
Mot_num_B = 0

''' Define Touchscreen Parameters '''

## Pin Object for x_minus reading on the touchscreen
pin_xm = pyb.Pin(pyb.Pin.cpu.A6)

## Pin Object for x_plus reading on the touchscreen
pin_xp = pyb.Pin(pyb.Pin.cpu.A0)

## Pin Object for y_minus reading on the touchscreen
pin_ym = pyb.Pin(pyb.Pin.cpu.A7)

## Pin Object for y_plus reading on the touchscreen
pin_yp = pyb.Pin(pyb.Pin.cpu.A1)

## Length of the touchscreen (x direction)
length = 176    # [mm]

## Width of the touchscreen (y direction)
width = 99.36   # [mm]

''' Define program tasks '''

## Task Object that sets-up the encoder for Motor A
task1 = Encoder(timer_ENC_A, pin1, pin1_ch, pin2, pin2_ch, counter_size, PPR, rpm)

## Task Object that sets-up the encoder for Motor B
task2 = Encoder(timer_ENC_B, pin3, pin3_ch, pin4, pin4_ch, counter_size, PPR, rpm)

## Task Object that sets-up Motor A
task3 = Motor(nSLEEP_pin, IN1_pin, IN1_ch, IN2_pin, IN2_ch, timer_MOT, Mot_num_A, dbg)

## Task Object that sets-up Motor B
# task4 = Motor(nSLEEP_pin, IN3_pin, IN3_ch, IN4_pin, IN4_ch, timer_MOT, Mot_num_B, dbg)

## A task that uses the TouchScreen class to collect touchscreen data
task5 = TouchScreen(pin_xm, pin_xp, pin_ym, pin_yp, length, width)


#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
  #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #
#   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #   #


''' Controller '''

# Initialize I2C communication with imu sensor
i2c = machine.I2C(1)
imu = BNO055(i2c)

# Calibrate imu sensor
print('---------------------------------------------')
print('Calibrated: sys {} gyro {} accel {} mag {}'.format(*imu.cal_status()))
print('Level the platform by hand')
pyb.delay(2000)

i = 0

# Level platform by hand
while True:
    pyb.delay(250)
    print('Gyro      x {:5.0f}    y {:5.0f}     z {:5.0f}'.format(*imu.gyro()))
    print('Accel     x {:5.1f}    y {:5.1f}     z {:5.1f}'.format(*imu.accel()))
    # accel_data = [0.0,0.0,0.0]
    accel_data = list(imu.accel())
    # print("accel data is " + str(accel_data))
    if accel_data[0] >= -0.1 and accel_data[0] <= 0.1 and accel_data[1] >= -0.1 and accel_data[1] <= 0.1:
        i += 1
        if i == 3:
            print('Surface Leveled')
            print('---------------------------------------------')
            break
        else:
            pass
    else:
        pass

# Zero encoders
task1.set_position(0)
# task2.set_position(0)

# Define variables
x_dot        = 0             # [m/s]
y_dot        = 0             # [m/s]
theta_dot_y  = 0             # [deg/s]
theta_dot_x  = 0             # [deg/s]
x            = 0             # [m]
y            = 0             # [m]
theta_y      = 0             # [deg]
theta_x      = 0             # [deg]
R            = 2.21          # [Ohm]
K_T          = 13.8*10**(-3)  # [N-m/A]
V_DC         = 12            # [V]
K_duty = 100*R/(K_T*V_DC)    # 1334.5 [%/N-m]

# Define controllers gains
K1 = 0
# K2 = 2.021*10**3
K2 = -0.02
K3 = 0
# K4 = -39.66
K4 = -0.2

# Get encoder update period and enable motors
period = task1.period_max
task3.enable()
# task4.enable() 

print('WARNING! Board will be active in 3 seconds')
pyb.delay(3000)       

while True:
    # Run code at slowest period necessary to update encoders
    pyb.delay(period)
    try:
        # Check if there is a motor error
        if task3.nFault == True:
            task3.Error()
        else:
            # Update encoders
            task1.update()
            # task2.update()
            
            # Get touchscreen data
            scan_data = task5.Scan_XYZ()
            
            # If something is touching the touchscreen, proceed
            # if scan_data[0] == True:
            # Get ball's position and speed
            x_new = scan_data[2]
            # y_new = scan_data[1]
            x_dot = (x_new - x)/period
            # y_dot = (y_new - y)/period
            x = x_new
            # y = y_new
            
            # Get platform's angle and angular rotational speed
            theta_y_new = task1.get_position()
            # theta_x_new = task2.get_position()
            theta_dot_y = (theta_y_new - theta_y)/period
            # theta_dot_x = (theta_x_new - theta_x)/period
            theta_y = theta_y_new
            # theta_x = theta_x_new
            
            # Calculate necessary duty cycle for each motor to balance platform
            Torque_A = K1*x_dot + K2*theta_dot_y + K3*x + K4*theta_y  # [N-m]
            # Torque_B = -K1*y_dot - K2*theta_dot_x - K3*y - K4*theta_x  # [N-m]
            Duty_cycle_A = int(K_duty*Torque_A)  # [%]
            if abs(Duty_cycle_A) > 100:
                if Duty_cycle_A >= 0:
                    Duty_cycle_A = 100
                else:
                    Duty_cycle_A = -100
            else:
                pass
            print('theta: {:}, theta_dot: {:}, duty_cycle: {:}'.format(theta_y, theta_dot_y, Duty_cycle_A))
            # Duty_cycle_B = int(K_duty*Torque_A)  # [%]
            task3.set_duty(Duty_cycle_A)
            # task4.set_duty(Duty_cycle_B)
            
            # Check if there is a motor error
            if task3.nFault == True:
                task3.Error()
            else:
                pass
            # else:
            #     pass
    except:
        task3.disable()
        # task4.disable()
        break

