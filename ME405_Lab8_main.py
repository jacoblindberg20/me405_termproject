'''
@file ME405_Lab8_main.py

@brief This file runs the Motor and Encoder drivers in ME405_lab8.py.

@author Dakota Baker
@date March 6, 2021
'''

import pyb
from ME405_Lab8 import Motor
from ME405_Lab8 import Encoder
import machine
from bno055 import *
import micropython
import machine
import time
from bno055 import *

# Allocates 200 bytes for an emergency error message
micropython.alloc_emergency_exception_buf(200)


''' Define Encoder parameters '''

## Pyboard timer object
timer_ENC_A = 4

## Pin object 1
pin1 = pyb.Pin(pyb.Pin.cpu.B6)

## Channel of timer for Pin Object 1
pin1_ch = 1

## Pin Object 2
pin2 = pyb.Pin(pyb.Pin.cpu.B7)

## Channel of timer for Pin Object 2
pin2_ch = 2

## Pyboard timer object
timer_ENC_B = 8

## Pin object 3
pin3 = pyb.Pin(pyb.Pin.cpu.C6)

## Channel of timer for Pin Object 3
pin3_ch = 1

## Pin Object 4
pin4 = pyb.Pin(pyb.Pin.cpu.C7)

## Channel of timer for Pin Object 4
pin4_ch = 2

## Size of counter (bits). The Nucelo has a 16-bit counter
counter_size = 16 # bit size

## Pulse per revolution for user's encoder
PPR = 4000 # Pulse per revolution

## Maximum revolutions per minute for motor
rpm = 12400 # maximum rpm of motor (Note: output shaft will be 4x faster)

''' Define Motor parameters '''

## Pin object used to enable the motor
nSLEEP_pin = pyb.Pin.cpu.A15

## Pin object for input 1 to half-bridge 
IN1_pin = pyb.Pin.cpu.B4

## Pin object for input 2 channel to half-bridge
IN1_ch = 1

## Pin object for input 2 to half-bridge 
IN2_pin = pyb.Pin.cpu.B5

## Pin object for input 2 channel to half-bridge
IN2_ch = 2

## Pin object for input 3 to half-bridge 
IN3_pin = pyb.Pin.cpu.B0

## Pin object for input 3 channel to half-bridge
IN3_ch = 3

## Pin object for input 4 to half-bridge 
IN4_pin = pyb.Pin.cpu.B1

## Pin object for input 4 channel to half-bridge
IN4_ch = 4

## Create the timer object used for PWM generation
timer_MOT = 3

## Create the debug object. 1 = print debug statements, 0 = no debug statements
dbg = 1

## Defines Motor A so the nFault interrupt only gets defined once.
Mot_num_A = 1

## Defines Motor B so the nFault interrupt only gets defined for Motor A.
Mot_num_B = 0


''' Define program tasks '''

## Task Object that sets-up the encoder for Motor A
task1 = Encoder(timer_ENC_A, pin1, pin1_ch, pin2, pin2_ch, counter_size, PPR, rpm)

## Task Object that sets-up the encoder for Motor B
task2 = Encoder(timer_ENC_B, pin3, pin3_ch, pin4, pin4_ch, counter_size, PPR, rpm)

## Task Object that sets-up Motor A
task3 = Motor(nSLEEP_pin, IN1_pin, IN1_ch, IN2_pin, IN2_ch, timer_MOT, Mot_num_A, dbg)

## Task Object that sets-up Motor B
task4 = Motor(nSLEEP_pin, IN3_pin, IN3_ch, IN4_pin, IN4_ch, timer_MOT, Mot_num_B, dbg)


i2c = machine.I2C(1)
imu = BNO055(i2c)
pyb.delay(1000)

print('Calibration required: sys {} gyro {} accel {} mag {}'.format(*imu.cal_status()))
print('Gyro      x {:5.0f}    y {:5.0f}     z {:5.0f}'.format(*imu.gyro()))
print('Accel     x {:5.1f}    y {:5.1f}     z {:5.1f}'.format(*imu.accel()))

while True:
    print('Gyro      x {:5.0f}    y {:5.0f}     z {:5.0f}'.format(*imu.gyro()))
    print('Accel     x {:5.1f}    y {:5.1f}     z {:5.1f}'.format(*imu.accel()))
    accel_data = [0.0,0.0,0.0]
    accel_data = list(imu.accel())
    print("accel data is " + str(accel_data))
    #new_accel_data = (accel_data[0], accel_data[1], accel_data[2]).(*imu.accel())
    if accel_data[0] >= -0.1 and accel_data[0] <= 0.1 and accel_data[1] >= -0.1 and accel_data[1] <= 0.1:
        print('Surface Leveled')
        break
    else:
        pass

    

print("LEVELED BRO!!")


'''
period = task1.period_max
print('Period = {:} ms'.format(period))
pyb.delay(2000)
task3.enable()
task4.enable()
task3.set_duty(50)
task4.set_duty(50)
=======
# Pyboard hardware I2C
i2c = machine.I2C(1)
imu = BNO055(i2c)
pyb.delay(1000)  # miliseconds
print('Calibrated: sys {} gyro {} accel {} mag {}'.format(*imu.cal_status()))
print('Gyro      x {:5.0f}    y {:5.0f}     z {:5.0f}'.format(*imu.gyro()))
print('Accel     x {:5.1f}    y {:5.1f}     z {:5.1f}'.format(*imu.accel()))

>>>>>>> be0948e4007b654cc4374725f975c0facdd2a4a9
while True:
    print('Gyro      x {:5.0f}    y {:5.0f}     z {:5.0f}'.format(*imu.gyro()))
    print('Accel     x {:5.1f}    y {:5.1f}     z {:5.1f}'.format(*imu.accel()))
    accel_data = *imu.accel()
    if (accel_data[0] & accel_data[1]) >= -0.1 & (accel_data[0] & accel_data[1]) <= 0.1:
        print('Surface Leveled')
        break
<<<<<<< HEAD
'''
        

# period = task1.period_max
# task3.enable()
# task4.enable()




# while True:
#     try:
#         pyb.delay(period)
#         print(task3.nFault)
#         if task3.nFault == True:
#             task3.Error()
#         else:
#             task1.update()
#             task2.update()
#             # print('{:}, {:}'.format(task1.current_position, task2.current_position))
#     except KeyboardInterrupt:
#         task3.disable()
#         task4.disable()
#         break

