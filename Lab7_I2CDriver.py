'''
@file Lab7_IMUDriver.py
@brief Main file used to drive the BNO0855 IMU Breakout Sensor Module
@details This class serves as the main file that runs the driver for the IMU
        breakout board Mo: BNO0855. IMU data is pulled at a frequency great enough
        for the main file to act on the data in fractions of a millisecond, so that
        the motors can react fast enough to keep the balanced ball atop the platform
        from sliding off.
@author Jacob Lindberg
@date March 7, 2021
''' 

######## IMPORT NECESSARY MODULES ###########
from pyb import I2C
import utime



class BNO055:

    ###### CONSTRUCTOR ARGUMENTS ########
    # address = 0x28
    # SDA_Pin = pyb.Pin.board.PB9
    # SCL_Pin = pyb.Pin.board.PB8
    
    #### Data Registers ####
    ## Accelerometer Data Register
    ACC_DATA = 0x08  
    ## Masgnetometer Data Register
    MAG_DATA = 0x0E
    ## Gyroscope Data Register
    GYRO_DATA = 0x14
    
    #### Configuration Registers ####
    ## Accelerometer Config. Register
    ACC = 0x08  # Registers for configuration (page 1)
    ## Magnetometer Config. Register
    MAG = 0x09
    ## Gyroscope Config. Register
    GYRO = 0x0a
    
    def __init__(self, address, SDA_Pin = pyb.Pin.board.PB9, SCL_Pin = pyb.Pin.board.PB8):
        
        ## Initialize I2C device within Nucleo on Bus 1 as Master Mode
        self.i2c = I2C(1, I2C.MASTER)
        ## Register Address for the BNO0855
        self.address = address
        ## Chip ID Rgister for Indexing
        self.Chip_ID = 0xa0
        ## Calibration Register
        self.Calibration_Reg = 0x35
        ## Location for I2C chip data scanning
        self.ID_Reg = 0x00
        ## Pin for SDA
        self.SDA = SDA_Pin
        ## Pin for SCL
        self.SCL = SCL_Pin
        ## Initialize the communication between the board
        self.init()
        
        ###### Scan the chip for the right register device ######
        
        try:
            self._i2c.readfrom_mem_into(self.address, self.ID_Reg, buf = bytearray(1))
            scanned_I2C_reg = buf[0]
        except OSError:
            print("Error Configuring Device")
        
        if self.Chip_ID == scanned_I2C_reg:
            pass
        else:
            print("ID Register Check Failed. I2C deinitialized")
            self.i2c.deinit()
            
    def calibration(self, buf = bytearray(4)):
        '''
        @brief Calibration method used to scan device for proper data
        '''
        self._i2c.readfrom_mem_into(self.address, self.Calibration_Reg, data = bytearray(1))
        scanned_reg = data[0]
        
        buf[0] = (scanned_reg >> 6) & 0x03  # system buffer
        buf[1] = (scanned_reg >> 4) & 0x03  # gyroscope buffer
        buf[2] = (scanned_reg >> 2) & 0x03  # accelerometer buffer
        buf[3] = scanned_reg & 0x03  # magnetometer buffer
        
        return min(buf[1:]) == 3 and buf[0] > 0 # Return the byte array with 4 spots

        
        
            


